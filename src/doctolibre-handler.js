/*
* PROJET DOCTOLIBRE
*/

import axios from 'axios'
import dayjs from 'dayjs'
import secrets from './secrets'

const doctolibInfos = {
    agendaIds: '211143-52536-149671-245758-211142-252296-281648-302603-332140-52535-63550-90985-91976-217842' 
}

/*
{
  total: 0,
  availabilities: [
    { date: '20js minify20-09-15', slots: [] },
    { date: '2020-09-16', slots: [] },
    { date: '2020-09-17', slots: [] },
    { date: '2020-09-18', slots: [] }
  ],
  next_slot: '2020-09-21' // optionnal
}
*/

async function sendSms(text = 'Hello you!') {
    let ovh = require('ovh')({
        appKey: secrets.appKey,
        appSecret: secrets.appSecret,
        consumerKey: secrets.consumerKey
      });

    const now = dayjs().hour()
    if (!(now < 5 || now > 23)) {
        // Get the serviceName (name of your sms account)
        ovh.request('GET', '/sms', function (err, serviceName) {
            if (err) {
                console.log('sending sms err', err, serviceName);
            }
            else {
                console.log("My account SMS is " + serviceName);
                  
                // Send a simple SMS with a short number using your serviceName
                ovh.request('POST', '/sms/' + serviceName + '/jobs', {
                    message: text,
                    senderForResponse: true,
                    receivers: [secrets.targetPhone]
                }, function (errsend, result) {
                    console.log(errsend, result);
                });
            }
        });
    }
}

async function getSlots(date = '2020-09-20') {
    return axios.get('https://www.doctolib.fr/availabilities.json', {
        params: {
            start_date: date,
            visit_motive_ids: 1738042,
            agenda_ids: doctolibInfos.agendaIds,
            insurance_sector: 'public',
            practice_ids: 20936,
            limit: 10,
            telehealth: false,
            profileId: 37775,
            isOrganization: true,
            telehealthFeatureEnabled: false
        }
    })
}

async function getOtherDermato() {
    const resp = await axios.get('https://www.doctolib.fr/availabilities.json', {
        params: {
            start_date: dayjs().format('YYYY-MM-DD'),
            visit_motive_ids: 1738042,
            agenda_ids: '143144',
            insurance_sector: 'public',
            practice_ids: 55187,
            limit: 10,
            telehealth: false,
            profileId: 92934,
            isOrganization: true,
            telehealthFeatureEnabled: false
        }
    })
    return resp.data.message || 'GOGOGO'
}
async function testSlotsRequest(dateTime = '2020-11-09T11:15:00.000+01:00') {
    return axios.post('https://www.doctolib.fr/appointments.json', {
        agenda_ids: doctolibInfos.agendaIds,
        practice_ids: [
            20936
        ],
        appointment: {
            start_date: dateTime,
            visit_motive_ids: "1738042",
            profile_id: 37775,
            source_action: "profile"
        }
    })
    // return response.data
}

function extractSlots(availabilities) {
    let slots = []
    for (let av of availabilities) {
        if (av.slots.length !== 0) {
            slots = slots.concat(av.slots)
        }
    }
    return slots
}

async function getAllSlots() {
    let slots = []
    // let nextAvailable = dayjs()
    let requests = []
    let parallelRequests = 9
    // On requête par périodes de 30 jours, jusqu'à 180 jours
    for (let i = 0; i < 180; i += 10) {
        let dayRequested = dayjs().add(i, 'day')

        // if (!dayRequested.isBefore(nextAvailable)) {
        if (requests.length < parallelRequests) {
            requests.push(getSlots(dayRequested.format('YYYY-MM-DD')))
        } else {
            // console.log('request for', dayRequested.format('YYYY-MM-DD'))
            let datas = await axios.all(requests)
            requests = [getSlots(dayRequested.format('YYYY-MM-DD'))]
            datas = datas.map(r => r.data)

            for (let resp of datas) {
                slots = slots.concat(extractSlots(resp.availabilities))

                if (resp.reason !== undefined && resp.reason === 'not_opened_availability') {
                    break
                }
            }
        }
    }
    return slots
}

async function testSlots(slots) {
    let slotsRequests = []
    let slotsStatus = {}
    let slotsToSend = []

    for (const slot of slots) {
        slotsRequests.push(testSlotsRequest(slot))
    }
    let responses = await axios.all(slotsRequests)
    responses = responses.map(res => res.data)

    for (let resIdx in responses) {
        if (responses[resIdx].error === undefined) {
            slotsStatus[slots[resIdx]] = 'DISPO'
            slotsToSend.push(slots[resIdx])
        } else {
            slotsStatus[slots[resIdx]] = responses[resIdx].error
        }
    }

    if (slotsToSend.length !== 0) {
        console.log(dayjs().format(), 'Sending sms with: ', slotsToSend.join(' '))
        sendSms(`Disponitibilitées dermato ${slotsToSend.join(' ')}`)
    }

    return slotsStatus

}

async function main() {
    let slots = []
    let slotsStatus = {}

    console.log(dayjs().format(), 'Récupération des horaires sur les 6 prochains mois ...')
    slots = await getAllSlots()
    

    //console.log('\r')
    //console.log('-----------------------------------------------------------------------------------------------------------------')
    //console.log('Horaires dispo sur https://www.doctolib.fr/cabinet-medical/lyon/centre-de-dermatologie-bichat-confluence :')
    console.log(dayjs().format(), "Nbr d'horaires :", slots.length)
    //console.log('-----------------------------------------------------------------------------------------------------------------')
    //console.log('\r\r')


    console.log(dayjs().format(), 'Test des creneaux dispos ...')
    slotsStatus = await testSlots(slots)

    slotsStatus['Plan B: Dr Muriel Delcombel'] = await getOtherDermato()

    //console.log('\r')
    //console.log('-----------------------------------------------------------------------------------------------------------------')
    console.log(dayjs().format(), 'Résultat des tests de créneaux :', Object.values(slotsStatus).join(', '))
    //console.table(slotsStatus)
    //console.log('-----------------------------------------------------------------------------------------------------------------')
    return slotsStatus
}

exports.handler = async function(event, context) {
    console.log(event);
    return {
        body: JSON.stringify(await main(), undefined, 2),
        statusCode: 200,
    }
}

main()
