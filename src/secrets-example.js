// Instructions on https://docs.ovh.com/fr/sms/envoyer_des_sms_avec_lapi_ovh_en_nodejs/
const targetPhone = '$PHONE'
const scriptName = '$SCRIPT_NAME'
const appKey = '$APP_KEY'
const appSecret = '$APP_SECRET'
const consumerKey = '$CONSUMER_KEY'

exports.targetPhone = targetPhone
exports.appKey = appKey
exports.appSecret = appSecret
exports.consumerKey = consumerKey
