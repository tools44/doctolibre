# Doctolibre

Projet utilitaire pour être averti des disponibilités sur doctolib via SMS.

Déploiement sur les lmabdas "serverless", que ce soit chez AWS, scaleway, ...

Env cible : node 10+ (donc adieu es6 "out-of-the-box")

L'utilisation de webpack sera un test. Pas de nodeExternals, il devra produire un seul fichier exécutable avec node 10 autosiffsant.

## RODMAP

typescript, linter
