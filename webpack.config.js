var path = require('path');
var nodeExternals = require('webpack-node-externals');

module.exports = {
    target: 'node',
    mode: 'production',
    entry: ['babel-polyfill', './src/doctolibre-handler.js'],
    output: {
        path: path.resolve(__dirname, 'dist/'),
        filename: 'doctolibre-handler.js'
    },
    externals: [],
    module: {
        rules: [{
            test: /.js$/,
            // exclude: /(node_modules|bower_components)/,
            use: {
                loader: 'babel-loader',
                options: {
                    presets: ["@babel/preset-env"]
                }
            }
        }]
    },
    stats: {
        colors: true
    }
};
